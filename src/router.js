import Vue from 'vue'
import VueRouter from 'vue-router'
import MainPage from "@/pages/MainPage";
import FilmPage from "@/pages/FilmPage";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        component: MainPage,
    },
    {
        path: '/films/:url',
        name: 'film',
        component: FilmPage,
        props: true
    },
]

export default new VueRouter({
    mode: 'history',
    routes
})